// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

/**
 * This artifact provides an <a href="http://www.slf4j.org">SLF4J</a> (version
 * &lt; 2.0) binding ("logging provider") in order to make, for example, your
 * <a href="http://tomcat.apache.org">Tomcat</a> or
 * <a href="http://projects.spring.io/spring-boot">Spring Boot</a> driven
 * applications log, amongst others, with the fancy <a href=
 * "https://www.javadoc.io/doc/org.refcodes/refcodes-logger-alt-console/latest/org.refcodes.logger.alt.console/org/refcodes/logger/alt/console/package-summary.html">refcodes-logger-alt-console</a>
 * console artifact or to a
 * <a href="http://en.wikipedia.org/wiki/NoSQL">NoSQL</a> DB cluster as of the
 * <a href=
 * "https://www.javadoc.io/doc/org.refcodes/refcodes-logger-alt-simpledb/latest/org.refcodes.logger.alt.simpledb/org/refcodes/logger/alt/simpledb/package-summary.html">refcodes-logger-alt-simpledb</a>
 * artifact.
 * 
 * <p style="font-style: plain; font-weight: normal; background-color: #f8f8ff;
 * padding: 1.5rem; border-style: solid; border-width: 1pt; border-radius: 10pt;
 * border-color: #cccccc;text-align: center;">
 * Please refer to the <a href=
 * "https://www.metacodes.pro/refcodes/refcodes-logger"><strong>refcodes-logger:
 * Fancy runtime-logs and highly scalable cloud logging</strong></a>
 * documentation for an up-to-date and detailed description on the usage of this
 * artifact.
 * </p>
 */
package org.slf4j.impl;
