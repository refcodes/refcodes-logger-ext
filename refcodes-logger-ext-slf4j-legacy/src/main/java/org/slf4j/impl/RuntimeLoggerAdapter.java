// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////
package org.slf4j.impl;

import org.refcodes.exception.BugException;
import org.refcodes.logger.LogPriority;
import org.refcodes.logger.RuntimeLogger;
import org.refcodes.logger.RuntimeLoggerAccessor;
import org.slf4j.Marker;
import org.slf4j.helpers.MarkerIgnoringBase;
import org.slf4j.spi.LocationAwareLogger;

/**
 * The Class RuntimeLoggerAdapter.
 */
public final class RuntimeLoggerAdapter extends MarkerIgnoringBase implements RuntimeLoggerAccessor, LocationAwareLogger {

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	// final static String FQCN = RuntimeLoggerAdapter.class.getName();

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	final transient RuntimeLogger logger;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new runtime logger adapter.
	 *
	 * @param logger the logger
	 */
	public RuntimeLoggerAdapter( RuntimeLogger logger ) {
		this.logger = logger;
		this.name = logger.getName();
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isTraceEnabled() {
		return logger.isLogTrace();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void trace( String msg ) {
		logger.trace( msg );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void trace( String format, Object arg ) {
		logger.trace( format, arg );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void trace( String format, Object arg1, Object arg2 ) {
		logger.trace( format, arg1, arg2 );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void trace( String format, Object... args ) {
		logger.trace( format, args );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void trace( String msg, Throwable t ) {
		logger.trace( msg, t );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isDebugEnabled() {
		return logger.isLogDebug();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void debug( String msg ) {
		logger.debug( msg );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void debug( String format, Object arg ) {
		logger.debug( format, arg );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void debug( String format, Object arg1, Object arg2 ) {
		logger.debug( format, arg1, arg2 );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void debug( String format, Object... args ) {
		logger.debug( format, args );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void debug( String msg, Throwable t ) {
		logger.debug( msg, t );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isInfoEnabled() {
		return logger.isLogInfo();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void info( String msg ) {
		logger.info( msg );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void info( String format, Object arg ) {
		logger.info( format, arg );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void info( String format, Object arg1, Object arg2 ) {
		logger.info( format, arg1, arg2 );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void info( String format, Object... args ) {
		logger.info( format, args );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void info( String msg, Throwable t ) {
		logger.info( msg, t );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isWarnEnabled() {
		return logger.isLogWarn();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void warn( String msg ) {
		logger.warn( msg );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void warn( String format, Object arg ) {
		logger.warn( format, arg );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void warn( String format, Object arg1, Object arg2 ) {
		logger.warn( format, arg1, arg2 );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void warn( String format, Object... args ) {
		logger.warn( format, args );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void warn( String msg, Throwable t ) {
		logger.warn( msg, t );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isErrorEnabled() {
		return logger.isLogError();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void error( String msg ) {
		logger.error( msg );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void error( String format, Object arg ) {
		logger.error( format, arg );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void error( String format, Object arg1, Object arg2 ) {
		logger.error( format, arg1, arg2 );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void error( String format, Object... args ) {
		logger.error( format, args );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void error( String msg, Throwable t ) {
		logger.error( msg, t );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void log( Marker aMarker, String aCallerFQCN, int aLevel, String aMessage, Object[] aArgs, Throwable aThrowable ) {
		final LogPriority thePriority;
		switch ( aLevel ) {
		case LocationAwareLogger.TRACE_INT -> thePriority = LogPriority.TRACE;
		case LocationAwareLogger.DEBUG_INT -> thePriority = LogPriority.DEBUG;
		case LocationAwareLogger.INFO_INT -> thePriority = LogPriority.INFO;
		case LocationAwareLogger.WARN_INT -> thePriority = LogPriority.WARN;
		case LocationAwareLogger.ERROR_INT -> thePriority = LogPriority.ERROR;
		default -> throw new BugException( "Missing case statement for <" + aLevel + "> in implementation!" );
		}
		logger.log( thePriority, aMessage, aThrowable, aArgs );
	}

	/**
	 * In case the <code>Slf4jRuntimeLogger</code> (as created by the
	 * <code>Slf4jRuntimeLoggerFactorySingleton</code>) detects that SLF4J has
	 * bound a {@link RuntimeLoggerAdapter} (i.e. the REFCODES.ORG SLF4J
	 * binding), it directly delegates its method calls to the wrapped
	 * {@link RuntimeLogger} instead of marshaling a log request through this
	 * {@link RuntimeLoggerAdapter}; as marshaling would mean consolidating of
	 * various detailed {@link LogPriority} levels to a single SLF4J log level.
	 * 
	 * @return The wrapped {@link RuntimeLogger} used by the
	 *         <code>Slf4jRuntimeLogger</code> when possible.
	 */
	@Override
	public RuntimeLogger getRuntimeLogger() {
		return logger;
	}
}
