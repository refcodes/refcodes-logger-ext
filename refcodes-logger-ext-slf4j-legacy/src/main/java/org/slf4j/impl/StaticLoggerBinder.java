// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////
package org.slf4j.impl;

import org.slf4j.ILoggerFactory;
import org.slf4j.spi.LoggerFactoryBinder;

/**
 * "The binding of LoggerFactory class with an actual instance of ILoggerFactory
 * is performed using information returned by this class."
 */
public class StaticLoggerBinder implements LoggerFactoryBinder {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final StaticLoggerBinder SINGLETON = new StaticLoggerBinder();

	public static final String REQUESTED_API_VERSION = "1.6.99"; // !final

	private static final String loggerFactoryClassStr = RuntimeLoggerFactory.class.getName();

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Gets the singleton.
	 *
	 * @return the singleton
	 */
	public static final StaticLoggerBinder getSingleton() {
		return SINGLETON;
	}

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final ILoggerFactory loggerFactory;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Creates a new {@link StaticLoggerBinder} instance.
	 */
	private StaticLoggerBinder() {
		loggerFactory = new RuntimeLoggerFactory();
		try {
			// @SuppressWarnings("unused")
			// Level level = Level.TRACE;
			Class.forName( "org.apache.log4j.Level" );
			// @SuppressWarnings("unused")
			// Level level = Level.TRACE;
		}
		catch ( ClassNotFoundException | NoSuchFieldError e ) {
			// Util.report( "This version of SLF4J requires log4j version <" + REQUESTED_API_VERSION + "> or later. See also http://www.slf4j.org/codes.html#log4j_version" );
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ILoggerFactory getLoggerFactory() {
		return loggerFactory;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getLoggerFactoryClassStr() {
		return loggerFactoryClassStr;
	}
}
