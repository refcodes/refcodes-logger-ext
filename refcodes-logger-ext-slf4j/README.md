# README #

> The [`REFCODES.ORG`](http://www.refcodes.org/refcodes) codes represent a group of artifacts consolidating parts of my work in the past years. Several topics are covered which I consider useful for you, programmers, developers and software engineers.

## What is this repository for? ##

***This artifact provides an [`SLF4J`](http://www.slf4j.org) (verion >= 2.0) binding ("logging provider") for SLF4J version >= `2.0` in order to make, for example, your [`Tomcat`](http://tomcat.apache.org/) or [`Spring Boot`](http://projects.spring.io/spring-boot) driven applications log, amongst others, with the fancy [`refcodes-logger-alt-console`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger-alt-console/latest/org.refcodes.logger.alt.console/org/refcodes/logger/alt/console/package-summary.html) console artifact or to a [`NoSQL`](http://en.wikipedia.org/wiki/NoSQL) DB cluster as of the [`refcodes-logger-alt-simpledb`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger-alt-simpledb/latest/org.refcodes.logger.alt.simpledb/org/refcodes/logger/alt/simpledb/package-summary.html) artifact.***

## Getting started ##

> Please refer to the [refcodes-logger: Fancy runtime-logs and highly scalable cloud logging](https://www.metacodes.pro/refcodes/refcodes-logger) documentation for an up-to-date and detailed description on the usage of this artifact.

## How do I get set up? ##

To get up and running, include the following dependency (without the three dots "...") in your `pom.xml`:


```
<dependencies>
	...
	<dependency>
		<artifactId>refcodes-logger-ext-slf4j2</artifactId>
		<groupId>org.refcodes</groupId>
		<version>3.3.9</version>
	</dependency>
	...
</dependencies>
```

The artifact is hosted directly at [Maven Central](http://search.maven.org). Jump straight to the source codes at [Bitbucket](https://bitbucket.org/refcodes/refcodes-logger-ext/src/master/refcodes-logger-ext-slf4j). Read the artifact's javadoc at [javadoc.io](http://www.javadoc.io/doc/org.refcodes/refcodes-logger-ext-slf4j).

## Contribution guidelines ##

* Writing tests
* Code review
* Adding functionality
* Fixing bugs

## Who do I talk to? ##

* Siegfried Steiner (steiner@refcodes.org)

## Terms and conditions ##

The [`REFCODES.ORG`](http://www.refcodes.org/refcodes) group of artifacts is published under some open source licenses; covered by the  [`refcodes-licensing`](https://bitbucket.org/refcodes/refcodes-licensing) ([`org.refcodes`](https://bitbucket.org/refcodes) group) artifact - evident in each artifact in question as of the `pom.xml` dependency included in such artifact.
