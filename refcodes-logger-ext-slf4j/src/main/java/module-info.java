module org.refcodes.logger.ext.slf4j {
	requires org.refcodes.runtime;
	requires jul.to.slf4j;
	requires transitive org.refcodes.logger;
	requires transitive org.slf4j;
	requires static org.refcodes.security.ext.chaos;
	// requires static org.refcodes.logger.alt.console;

	provides org.slf4j.spi.SLF4JServiceProvider with org.refcodes.logger.ext.slf4j.RuntimeLoggerServiceProvider;

	exports org.refcodes.logger.ext.slf4j;
}
